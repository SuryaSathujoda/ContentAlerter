from datetime import datetime
from bs4 import BeautifulSoup
import urllib2
import re

def write_log(chapter, entry):
    log=open("../log/Chapter"+str(chapter)+".log","a")
    log.write(entry)
    log.close()
    return

def write_latest_chapter(chapter):
    latest_chapter_update=open("latest_chapter", "w")
    latest_chapter_update.write(str(element))
    latest_chapter_update.close()
    return

request_url="http://mangastream.com/manga/one_piece"

latest_chapter=open("latest_chapter", "r")
prev_chapter_number=int(latest_chapter.readline())
chapter_number=prev_chapter_number+1
latest_chapter.close()

log_entry="LOG: Script started at: "+str(datetime.now())+"\n"
log_entry+="Sending request to '"+request_url+"'\n"
try:
    response=urllib2.urlopen(request_url)
except urllib2.HTTPError as err:
    log_entry+="An error has occured while trying to process request...\n"
    log_entry+="Error message: "+"'"+str(err.code)+": "+err.reason+"'\n\n\n"
    write_log(chapter_number, log_entry)
    quit()

log_entry+="Successfully received response from server\n"
log_entry+="Parsing response data...\n"
soup=BeautifulSoup(response.read(), from_encoding=response.info().getparam('charset'))
table=soup.table.find_all("a", href=True)

if(len(table)==0):
    log_entry+="Error parsing response data: No chapter table found\n\n\n"
    write_log(chapter_number, log_entry)
    quit()

chapter=str(chapter_number)+"\ (.+?)"
prev_chapter=str(prev_chapter_number)+"\ (.+?)"
chapter_compiled=re.compile(chapter)
prev_chapter_compiled=re.compile(prev_chapter)

incorrect_index_err=False
for record in table:
    if prev_chapter_compiled.match(record.string):
        incorrect_index_err=True

if(incorrect_index_err==False):
    log_entry+="An error has occured while checking for update:\n"
    log_entry+="Inproper indexing of chapter numbers\n"
    log_entry+="Previous chapter: "+str(prev_chapter_number)+" Not Found\n"
    log_entry+="Lastest chapter found: "+table[0].string+"\n"
    log_entry+=table[0]["href"]+"\n\n\n"
    for element in table[0].string.split():
        if element.isdigit():
            write_log(element, log_entry)
            write_latest_chapter(element)
    write_log(chapter_number, log_entry)
    quit()

for record in table:
    if chapter_compiled.match(record.string):
        log_entry+="There has been an update since last run\n"
        log_entry+="New chapter has been released: \n"
        log_entry+=record.string+"\n"
        log_entry+=record["href"]+"\n"
        for element in table[0].string.split():
            if element.isdigit():
                log_entry+="Latest Chapter is: "+table[0].string+"\n"
                log_entry+=table[0]["href"]+"\n\n\n"
                write_log(element, log_entry)
                write_latest_chapter(element)
                quit()
        log_entry+="\n\n"
        write_log(chapter_number, log_entry)
        latest_chapter=open("latest_chapter", "w")
        latest_chapter.write(str(chapter_number))
        latest_chapter.close()
        quit()

log_entry+="Completed parsing response data\n"
log_entry+="There has been no new release since last run for chapter "+str(chapter_number)+"\n\n\n"
write_log(chapter_number, log_entry)
